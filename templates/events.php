<?php 

/*
  
    Template Name: Events

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('templates/events/upcoming'); ?>

    <?php get_template_part('templates/events/recordings'); ?>

<?php get_footer(); ?>