<?php 

/*
  
    Template Name: Contact

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/page-header'); ?>

    <?php get_template_part('templates/contact/contact-info'); ?>

    <?php get_template_part('templates/contact/faqs'); ?>

    <?php get_template_part('templates/contact/contact-form'); ?>


<?php get_footer(); ?>