<?php
    $response = get_field('response');

    $copy = $response['copy'];
?>

<section class="response">
    <div class="copy">
        <?php echo $copy; ?>
    </div>
</section>